
@extends('layouts.app2')

@section('content')


<div class="container">
    <div class="row" style="padding-top:30px">
        <div class="col-md-10">        
            <form action="{{route('create')}}" method="post">
                @csrf
                <!-- @csrf token cros site untuk membuat lebih aman -->
                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" class="form-control" name="name" required>
                </div>
                <div class="form-group">
                    <label for="task">Task</label>
                    <textarea class="form-control" rows="3" name="task" required></textarea>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
</div>

@endsection