<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// example one with middleware

// Route::get('/', function () {
//     return view('welcome');
// });

// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
// Route::get('admin/home', 'HomeController@adminHome')->name('admin.home')->middleware('is_admin');


// example two without middleware

Route::get('/','AuthController@showFormLogin')->name('login');
Route::get('login','AuthController@showFormLogin')->name('login');
Route::post('login','AuthController@login');

Route::get('register', 'AuthController@showFormRegister')->name('register');
Route::post('register','AuthController@register');

Route::group(['middleware' => 'auth'], function() {
    Route::get('home','HomeController@index')->name('home');
    Route::post('logout','AuthController@logout')->name('logout');
    Route::get('todo/index', 'TodoController@ShowFormTodo')->name('todo');
    Route::post('todo/index','TodoController@ShowFormTodo');
    Route::get('todo/create', 'TodoController@ShowFormCreate')->name('create');
    Route::post('todo/create', 'TodoController@store'); 
    Route::get('todo/delete/{id}', 'TodoController@destroy')->name('destroy');
    Route::get('todo/view/{id}', 'TodoController@edit')->name('edit');
    Route::post('todo/update/{id}', 'TodoController@update')->name('update');
});