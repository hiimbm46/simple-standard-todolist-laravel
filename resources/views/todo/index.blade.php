
@extends('layouts.app2')

@section('content')

<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
        <div class="col-sm-6">
            <h1 class="m-0 text-dark">Halaman Todo</h1>
        </div>
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="/">Dashboard</a></li>
            <li class="breadcrumb-item">Todo List</li>
            </ol>
        </div>
        </div>
    </div>
</div>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <!-- Alert message (start) -->
                @if(Session::has('message'))
                <div class="alert {{ Session::get('alert-class') }}">
                    {{ Session::get('message') }}
                </div>
                @endif
                <!-- Alert message (end) -->
                <div class="card">
                    <div class="card-header">
                        <a class="btn btn-success"href="{{route('create')}}">Create Todo List</a>
                    </div>
                    <div class="card-body">
                        <div class="card-body table-responsive p-0">
                            <table class="table table-hover text-nowrap">
                                <thead>
                                <tr>
                                    <th colspan="5"> Name </th>
                                    <th colspan="5"> Task </th>
                                    <th colspan="5"> Status </th>
                                    <th colspan="4"> Action </th>
                                </tr>
                                </thead>
                                <tbody>
                                    <!-- count untuk mengecek berapa isi dari array -->
                                    @if(count($todo) > 0 )  
                                    <!-- pemanggilan variable $todo bedasarkan key -->
                                    @foreach($todo as $row)
            
                                    <tr>
                                        <td colspan="5">{{$row['name']}}</td>
                                        <td colspan="5">
                                            {{$row['task']}}
                                        </td>
                                        <td colspan="5">
                                            @if ($row->status == '0')
                                                <div class="badge badge-danger ml-2">Unfinished</div>
                                            @else
                                                <div class="badge badge-success ml-2">Finished</div>
                                            @endif
                                        </td>
                                        <td colspan="4">
                                            <a class="btn btn-warning"href="{{route('edit',[$row->id])}}">Update Todo List</a>
                                            <a class="btn btn-danger"href="{{route('destroy',[$row->id])}}">Delete Todo List</a>
                                        </td>
                                    </tr>
                                    @endforeach
                                    @else 
                                        Sorry Data is Not available
                                        <style>
                                            thead {
                                                display:none !important;
                                            }
                                        </style>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection