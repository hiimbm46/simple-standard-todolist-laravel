<?php

namespace App\Http\Controllers;

use App\Todo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;




class TodoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function ShowFormTodo()
    {
        // $todos = Todo::all();
        // return view('todo.index', compact('todos'));

        $todos = Todo::all();
        return view('todo.index', [
            'todo' => $todos
            // key => value
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function ShowFormCreate()
    {
        
        return view('todo.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //untuk menyimpan data ke dalam database

        $request->validate([
            'name'=>'required',
            'task'=>'required',
        ]);
        
        $todo = new Todo;
        $todo->name = $request->name ;
        $todo->task = $request->task;
        $todo->status = false;
        $todo->save();

        Session::flash('message', 'Create Data successfully!');
        Session::flash('alert-class', 'alert-success');
        return redirect()->route('todo');

        // Todo::create([
        //     'name' => $request->name,
        //     'task' => $request->task,
        // ]);
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Todo  $todo
     * @return \Illuminate\Http\Response
     */
    public function show(Todo $todo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Todo  $todo
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $todos = Todo::find($id);
        return view('todo.edit',[
            'todo' => $todos
        ]);

       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Todo  $todo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
            'task' => 'required|string',
            'status' => 'string'
        ]);

        $todo = Todo::find($id);
        $todo->name = $request->name ;
        $todo->task = $request->task;
        $todo->status = $request->status;
        $todo->save();


        if ($validator->fails()) {
            return redirect()->Back()->withInput()->withErrors($validator);
        }
        if($todo->update()){

            Session::flash('message', 'Update successfully!');
            Session::flash('alert-class', 'alert-success');
            return redirect()->route('todo',[
                'todo' => $todo

            ]);
        }else{
            Session::flash('message', 'Data not updated!');
            Session::flash('alert-class', 'alert-danger');
        }
        return Back()->withInput();
        }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Todo  $todo
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Todo::destroy($id);
        Session::flash('message', 'Delete successfully!');
        Session::flash('alert-class', 'alert-success');
        return redirect()->route('todo');
    }
}
