@extends('layouts.app2')

@section('content')


<div class="container">
    <h1 style="padding-top:30px;">Update {{old('name',$todo->name)}}</h1>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">

            <!-- Alert message (start) -->
            @if(Session::has('message'))
            <div class="alert {{ Session::get('alert-class') }}">
                {{ Session::get('message') }}
            </div>
            @endif
            <!-- Alert message (end) -->

            <form action="{{route('update',[$todo->id])}}" method="post" >
                @csrf
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Name <span class="required">*</span></label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="name" class="form-control col-md-12 col-xs-12" name="name" placeholder="Enter subject name" required="required" type="text" value="{{old('name',$todo->name)}}">
                        @if ($errors->has('name'))
                        <span class="errormsg">{{ $errors->first('name') }}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Task
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <textarea name='task' id='task' class='form-control' placeholder="Enter task">{{old('task',$todo->task)}}</textarea>
                        @if ($errors->has('task'))
                        <span class="errormsg">{{ $errors->first('task') }}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="status">Status</label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <select class="form-control" id="status" name="status">
                            <option value="0">Unfinished</option>
                            <option value="1">Done</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6">
                        <input type="submit" name="submit" value='Submit' class='btn btn-success'>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="row">

</div>

@endsection