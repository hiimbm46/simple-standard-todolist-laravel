<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
</head>

<style>
    .card {
        border: 1px solid transparent;
        background-color:#00000094;
        padding:35px
    }

    .card h3,label,p {
        color : #fff;
    }

    .card .card-header  {
        border-bottom:2px solid #fff;
    }

    .card .card-body  {
        border-bottom:2px solid #fff;
    }

    .card-footer button {
        width:50%;
        display:block;
        margin:10px auto;
        background-color: #0561c56b;
    }
</style>
<body style="background-image:url('https://collegeinfogeek.com/wp-content/uploads/2019/05/best-to-list-apps-featured-image.jpg'); background-size:cover; background-position:center; height:100%;">
    <div class="container">
        <div class="row">
            <div class="col-md-5 mt-5 ml-auto mr-auto">
                <div class="card">
                    <div class="card-header">
                        <h3 class="text-center">Form Login</h3>
                    </div>
                    <form action="{{ route('login') }}" method="post">
                    @csrf
                    <div class="card-body">
                        @if(session('errors'))
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                Something it's wrong:
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                                <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                                </ul>
                            </div>
                        @endif
                        @if (Session::has('success'))
                            <div class="alert alert-success">
                                {{ Session::get('success') }}
                            </div>
                        @endif
                        @if (Session::has('error'))
                            <div class="alert alert-danger">
                                {{ Session::get('error') }}
                            </div>
                        @endif
                        <div class="form-group">
                            <label for=""><strong>Email</strong></label>
                            <input type="text" name="email" class="form-control" placeholder="Email">
                        </div>
                        <div class="form-group">
                            <label for=""><strong>Password</strong></label>
                            <input type="password" name="password" class="form-control" placeholder="Password">
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary btn-block">Log In</button>
                        <p class="text-center">Belum punya akun? <a href="{{ route('register') }}">Register</a> sekarang!</p>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>
</html>